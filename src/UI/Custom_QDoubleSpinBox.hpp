#ifndef CUSTOM_QDOUBLESPINBOX_HEADER_HPP
#define CUSTOM_QDOUBLESPINBOX_HEADER_HPP

#include <QDoubleSpinBox>

class Custom_QDoubleSpinBox : public QDoubleSpinBox
{
  public:

  Custom_QDoubleSpinBox();
  QString textFromValue(double value) const;
};

#endif
