#include "Custom_QDoubleSpinBox.hpp"

Custom_QDoubleSpinBox::Custom_QDoubleSpinBox(): QDoubleSpinBox()
{
}

QString Custom_QDoubleSpinBox::textFromValue(double value) const
{
    return QLocale().toString(value, 'g', QLocale::FloatingPointShortest);
}
