#ifndef VIDEO_TAB_H
#define VIDEO_TAB_H

#include <QObject>
#include <QWidget>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QLineEdit>
#include <QPushButton>
#include <QLabel>
#include <QString>
#include <QDoubleValidator>
#include <QGroupBox>
#include <QFileDialog>
#include "constant_ui.hpp"
#include "Custom_QDoubleSpinBox.hpp"
#include "Custom_QPushButton.hpp"

class Video_Tab : public QWidget
{
    Q_OBJECT

private:

    QGroupBox zoom_box;
    QGroupBox point_box;
    QGroupBox create_box;
    QVBoxLayout layer_1;
    QVBoxLayout layer_2;
    QHBoxLayout layer_3;
    QHBoxLayout layer_4;
    QLabel zoom_min_title;
    QLabel zoom_max_title;
    QLabel zoom_step_title;
    QLabel point_title;
    QLabel x_title;
    QLabel y_title;
    QLabel create_title;
    QLabel save_title;
    Custom_QDoubleSpinBox zoom_min_value;
    Custom_QDoubleSpinBox zoom_max_value;
    Custom_QDoubleSpinBox zoom_step_value;
    Custom_QDoubleSpinBox x_value;
    Custom_QDoubleSpinBox y_value;
    Custom_QPushButton create_button;
    QPushButton save_button;

public:
    explicit Video_Tab(QWidget *parent = nullptr);
    virtual ~Video_Tab();

    void init_layers();
    void init_zoom();
    void init_point();
    void init_button();

    signals :

      void file_saved(QString &);

    public slots :

        void open_file_explorer();
};

#endif // VIDEO_TAB_H
