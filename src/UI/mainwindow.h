#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QRect>
#include <QDesktopWidget>
#include <QMainWindow>
#include <QTabWidget>
#include <QFile>
#include "image_tab.h"
#include "video_tab.h"
#include "image_viewer.h"
#include "video_viewer.h"

class MainWindow : public QMainWindow
{
    Q_OBJECT

private:

    QFile styleSheet_file;
    QRect size;
    QWidget central_widget;
    QHBoxLayout layer_1;
    QVBoxLayout layer_2;
    QVBoxLayout layer_3;
    QTabWidget onglets;
    Image_Tab image_tab;
    Video_Tab video_tab;
    Image_Viewer viewer1;
    Video_Viewer viewer2;

public:
    MainWindow(QWidget *parent = nullptr);
    void init_size();
    void init_tabs();
    void init_viewer();
    void init_layout();
    void init_stylesheet();
    virtual ~MainWindow();

public slots :

    void switch_viewer(int view);
};
#endif // MAINWINDOW_H
