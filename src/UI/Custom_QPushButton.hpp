#ifndef CUSTOM_QPUSHBUTTON_H
#define CUSTOM_QPUSHBUTTON_H

#include <QObject>
#include <QPushButton>
#include "constant_ui.hpp"

using namespace std;

class Custom_QPushButton : public QPushButton
{
  Q_OBJECT

  private :

  bool state;

  public:

  explicit Custom_QPushButton(QWidget *parent = nullptr);
  virtual ~Custom_QPushButton();

  void set_state(bool value);

  signals:

  void cancel_action();
  void launch_action();

  public slots:

  void change_state();
  void change_text();
};

#endif
