#include "image_viewer.h"

Image_Viewer::Image_Viewer(QWidget * parent):QLabel(parent),
    image()
{
    using namespace Image_Viewer_Constant;

    this->setMinimumSize(100,100);
    setScaledContents(false);
    setObjectName(image_viewer_name);
}

Image_Viewer::~Image_Viewer()
{

}

void Image_Viewer::setPixmap ( const QPixmap & p)
{
    image = p;
    QLabel::setPixmap(scaledPixmap());
    std::cout << "Image Displaying" << std::endl;
}

int Image_Viewer::heightForWidth( int width ) const
{
    return image.isNull() ? this->height() : ((qreal)image.height()*width)/image.width();
}

QSize Image_Viewer::sizeHint() const
{
    int w = this->width();
    return QSize( w, heightForWidth(w) );
}

QPixmap Image_Viewer::scaledPixmap() const
{
    return image.scaled(this->size(), Qt::KeepAspectRatio, Qt::SmoothTransformation);
}

void Image_Viewer::resizeEvent(QResizeEvent * e)
{
    if(!image.isNull())
        QLabel::setPixmap(scaledPixmap());
}
