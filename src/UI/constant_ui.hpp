#ifndef CONSTANT_UI_HEADER_H
#define CONSTANT_UI_HEADER_H

namespace Window_Constant
{
  enum class Layout_Space : int { Tab_Layout = 20 , Viewer_Layout = 80};
  constexpr const char default_stylesheet_path[] = "../ressource/stylesheet/Default.qss";
}

namespace Tab_Constant
{
  constexpr const char image_tab_title[] = "Image";
  constexpr const char video_tab_title[] = "Video";
  constexpr const char default_video_path[] = "/home/bruno/Documents/Mandelbrot_Fractal/ressource/video/video.avi";
  constexpr const char default_image_path[] = "../ressource/images/image1.png";
  constexpr bool default_visibility_image_viewer = true;
  constexpr bool default_visibility_video_viewer = false;

  constexpr bool visibility_on = true;
  constexpr bool visibility_off = false;

  enum class View_Selected : int {Image = 0, Video};
}

namespace Image_Tab_Constant
{
  constexpr const char image_tab_name[] = "Image_Tab";
  constexpr const char zoom_groupbox_title[] = "Zoom";
  constexpr const char zoom_value_name[] = "Zoom_Image_Name";
  constexpr const int zoom_placeholder_value = 1;
  constexpr const float min_zoom_validator = 0.001;
  constexpr const float max_zoom_validator = 1000000000;
  constexpr const int precision_zoom_validator = 3;
  constexpr const float zoom_minimum_width = 70;

  constexpr const char point_groupbox_title[] = "Target Point";
  constexpr const char x_coordonate_name[] = "X_Image_Name";
  constexpr const char x_coordonate_title[] = "x";
  constexpr const char y_coordonate_name[] = "Y_Image_Name";
  constexpr const char y_coordonate_title[] = "y";
  constexpr const float x_placeholder_value = -0.5;
  constexpr const float y_placeholder_value = 0;
  constexpr const float x_minimum_width = 70;
  constexpr const float y_minimum_width = 70;

  enum class X_Point_Validator : int { min = -2, max = 1, precision = 9};
  enum class Y_Point_Validator : int {min = -1, max = 1, precision = 9 };

  constexpr const char create_and_save_groupbox_title[] = "Create && Save";

  constexpr const char save_image_button_title[] = "Save";
  constexpr const char save_image_button_name[] = "Save_Image";
  constexpr const char create_image_button_title[] = "Create";
  constexpr const char cancel_image_button_title[] = "Cancel";
  constexpr const char create_image_button_name[] = "Create_Image";
  constexpr const char save_window_title[] = "Save Image";
  constexpr const char default_image_name[] = "mandelbrot_image.jpg";
  constexpr const char default_extension_file[] = ".jpg";
  constexpr const char default_caracter_extension[] = ".";
  constexpr const int indexOf_error = -1;
  constexpr const bool disabled_button_state = false;
  constexpr const bool enabled_button_state = true;

}

namespace Video_Tab_Constant
{
  constexpr const char video_tab_name[] = "Video_Tab";
  constexpr const char zoom_groupbox_title[] = "Zoom";
  constexpr const char zoom_min_title_value[] = "Min";
  constexpr const char zoom_max_title_value[] = "Max";
  constexpr const char zoom_step_title_value[] = "Step";
  constexpr const char zoom_min_value_name[] = "Zoom_Min_Video_Name";
  constexpr const char zoom_max_value_name[] = "Zoom_Max_Video_Name";
  constexpr const char zoom_step_value_name[] = "Zoom_Step_Video_Name";
  constexpr const float zoom_min_placeholder_value = 5;
  constexpr const float zoom_max_placeholder_value = 1;
  constexpr const float zoom_step_placeholder_value = 5;
  constexpr const float min_zoom_minimum_width = 70;
  constexpr const float max_zoom_minimum_width = 70;
  constexpr const float step_zoom_minimum_width = 70;

  namespace Min_Zoom
  {
    constexpr const double min = 0.001;
    constexpr const double max = 1000000000;
    constexpr const double precision = 3;
  }

  namespace Max_Zoom
  {
    constexpr const double min = 0.001;
    constexpr const double max = 1000000000;
    constexpr const double precision = 3;
  }

  namespace Step_Zoom
  {
    constexpr const double min = 1;
    constexpr const double max = 1000000;
    constexpr const double precision = 0;
  }

  constexpr const char point_groupbox_title[] = "Target Point";
  constexpr const char x_coordonate_name[] = "X_Video_Name";
  constexpr const char x_coordonate_title[] = "x";
  constexpr const char y_coordonate_name[] = "Y_Video_Name";
  constexpr const char y_coordonate_title[] = "y";
  constexpr const float x_placeholder_value = -0.5;
  constexpr const float y_placeholder_value = 0;
  constexpr const float x_minimum_width = 70;
  constexpr const float y_minimum_width = 70;

  enum class X_Point_Validator : int { min = -2, max = 1, precision = 9};
  enum class Y_Point_Validator : int {min = -1, max = 1, precision = 9 };

  constexpr const char create_and_save_groupbox_title[] = "Create && Save";

  constexpr const char save_video_button_title[] = "Save";
  constexpr const char save_video_button_name[] = "Save_Video";
  constexpr const char create_video_button_title[] = "Create";
  constexpr const char create_video_button_name[] = "Create_Video";
  constexpr const char save_window_title[] = "Save Video";
  constexpr const char default_video_name[] = "mandelbrot_video.avi";
}

namespace Video_Viewer_Constant
{
  constexpr const char video_viewer_name[] = "Video_Viewer";
  constexpr const char play_icon_path[] = "../ressource/images/play_icon.png";
  constexpr const char pause_icon_path[] = "../ressource/images/pause_icon.png";
  constexpr const char stop_icon_path[] = "../ressource/images/stop_icon.png";

  constexpr const char play_button_name[] = "play_button";
  constexpr const char pause_button_name[] = "pause_button";
  constexpr const char stop_button_name[] = "stop_button";

  constexpr const int icon_size = 50;
  constexpr const int restart_video_position = 0;
}

namespace Image_Viewer_Constant
{
  constexpr const char image_viewer_name[] = "image_viewer";
}

#endif
