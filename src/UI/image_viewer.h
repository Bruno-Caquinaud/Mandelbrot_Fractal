#ifndef IMAGE_VIEWER_H
#define IMAGE_VIEWER_H

#include <QObject>
#include <QWidget>
#include <QLabel>
#include <QPixmap>
#include <QResizeEvent>
#include <iostream>
#include "constant_ui.hpp"

class Image_Viewer : public QLabel
{
  Q_OBJECT

private:

    QPixmap image;

public:

    explicit Image_Viewer(QWidget * parent = nullptr);
    virtual ~Image_Viewer();
    virtual int heightForWidth( int width ) const;
    virtual QSize sizeHint() const;
    QPixmap scaledPixmap() const;

public slots:

    void setPixmap ( const QPixmap &);
    void resizeEvent(QResizeEvent *) override;

};

#endif
