#ifndef IMAGE_TAB_H
#define IMAGE_TAB_H

#include <QObject>
#include <QWidget>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QLineEdit>
#include <QLabel>
#include <QString>
#include <QDoubleValidator>
#include <QGroupBox>
#include <QFileDialog>
#include "constant_ui.hpp"
#include "Custom_QDoubleSpinBox.hpp"
#include "Custom_QPushButton.hpp"

class Image_Tab : public QWidget
{
  Q_OBJECT

private :

    QGroupBox zoom_box;
    QGroupBox point_box;
    QGroupBox create_and_save_box;
    QVBoxLayout layer_1;
    QHBoxLayout layer_2;
    QHBoxLayout layer_3;
    QHBoxLayout layer_4;
    QLabel zoom_title;
    QLabel point_title;
    QLabel x_title;
    QLabel y_title;
    QLabel create_title;
    QLabel save_title;
    Custom_QDoubleSpinBox zoom_value;
    Custom_QDoubleSpinBox x_value;
    Custom_QDoubleSpinBox y_value;
    Custom_QPushButton create_button;
    QPushButton save_button;
    QFileDialog file_explorer;

public:

    explicit Image_Tab(QWidget *parent = nullptr);
    virtual ~Image_Tab();

    void init_layer();
    void init_zoom();
    void init_point();
    void init_button();

signals :

    void file_saved(QString &);

public slots :

    void open_file_explorer();
};

#endif
