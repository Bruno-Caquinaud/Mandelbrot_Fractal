#include "video_viewer.h"

Video_Viewer::Video_Viewer(QWidget * parent):QWidget(parent),
    layer_1(), layer_2(), layer_3(),
    spacer_1(),
    play_button(), pause_button(),
    stop_button(), video_widget(), video_player()
{
    init_video_widget();
    init_video_player();
    init_button();
    init_layout();
}

Video_Viewer::~Video_Viewer()
{

}

void Video_Viewer::init_button()
{
    using namespace Video_Viewer_Constant;

    QPixmap play_pixmap(play_icon_path);
    QIcon play_icon(play_pixmap);
    QPixmap pause_pixmap(pause_icon_path);
    QIcon pause_icon(pause_pixmap);
    QPixmap stop_pixmap(stop_icon_path);
    QIcon stop_icon(stop_pixmap);

    play_button.setObjectName(QString(play_button_name));
    pause_button.setObjectName(QString(pause_button_name));
    stop_button.setObjectName(QString(stop_button_name));

    play_button.setIcon(play_icon);
    pause_button.setIcon(pause_icon);
    stop_button.setIcon(stop_icon);

    play_button.setIconSize(QSize(icon_size, icon_size));
    pause_button.setIconSize(QSize(icon_size, icon_size));
    stop_button.setIconSize(QSize(icon_size, icon_size));

    connect(&play_button, SIGNAL(clicked()), &video_player, SLOT(play()));
    connect(&pause_button, SIGNAL(clicked()), &video_player, SLOT(pause()));
    connect(&stop_button, SIGNAL(clicked()), this, SLOT(restart()));
}

void Video_Viewer::init_video_widget()
{
  QVideoSurfaceFormat format(QSize(800,800), QVideoFrame::Format_RGB24);
  video_widget.setAspectRatioMode(Qt::KeepAspectRatio);
}

void Video_Viewer::init_video_player()
{
    using namespace Video_Viewer_Constant;

    video_player.setVideoOutput(&video_widget);
}

void Video_Viewer::init_layout()
{
    using namespace Video_Viewer_Constant;

    layer_2.addWidget(&video_widget);
    layer_3.addWidget(&play_button);
    layer_3.addWidget(&pause_button);
    layer_3.addWidget(&stop_button);
    layer_1.addLayout(&layer_2);
    layer_1.addLayout(&layer_3);
    this->setLayout(&layer_1);

    this->setObjectName(video_viewer_name);
}

void Video_Viewer::set_media(QString & path)
{
    using namespace Video_Viewer_Constant;

    video_player.setMedia(QUrl::fromLocalFile(path));
    restart();
}

void Video_Viewer::restart()
{
    using namespace Video_Viewer_Constant;

    video_player.setPosition(restart_video_position);
}
