#include "video_tab.h"

Video_Tab::Video_Tab(QWidget *parent) : QWidget(parent),
    zoom_box(), point_box(), create_box(),
    layer_1(), layer_2(), layer_3(), layer_4(),
    zoom_min_title(), zoom_max_title(), zoom_step_title(),
    point_title(), x_title(), y_title(), create_title(),
    save_title(),
    zoom_min_value(), zoom_max_value(),
    zoom_step_value(), x_value(), y_value(), create_button(),
    save_button()
{
    init_zoom();
    init_point();
    init_button();
    init_layers();
}

Video_Tab::~Video_Tab()
{
}

void Video_Tab::init_layers()
{
    using namespace Video_Tab_Constant;

    layer_1.addWidget(&zoom_box);
    layer_1.addWidget(&point_box);
    layer_1.addWidget(&create_box);
    this->setLayout(&layer_1);
    this->setObjectName(video_tab_name);
}

void Video_Tab::init_zoom()
{
    using namespace Video_Tab_Constant;

    QString zoom(zoom_groupbox_title);
    QString zoom_min(zoom_min_title_value);
    QString zoom_max(zoom_max_title_value);
    QString zoom_step(zoom_step_title_value);
    QString zoom_min_name(zoom_min_value_name);
    QString zoom_max_name(zoom_max_value_name);
    QString zoom_step_name(zoom_step_value_name);

    zoom_min_title.setText(zoom_min);
    zoom_max_title.setText(zoom_max);
    zoom_step_title.setText(zoom_step);

    zoom_min_value.setRange(Min_Zoom::min, Min_Zoom::max);
    zoom_min_value.setDecimals(Min_Zoom::precision);
    zoom_min_value.setButtonSymbols(QAbstractSpinBox::NoButtons);
    zoom_min_value.setValue(zoom_min_placeholder_value);
    zoom_min_value.setMinimumWidth(min_zoom_minimum_width);

    zoom_max_value.setRange(Max_Zoom::min, Max_Zoom::max);
    zoom_max_value.setDecimals(Max_Zoom::precision);
    zoom_max_value.setButtonSymbols(QAbstractSpinBox::NoButtons);
    zoom_max_value.setValue(zoom_max_placeholder_value);
    zoom_max_value.setMinimumWidth(max_zoom_minimum_width);

    zoom_step_value.setRange(Step_Zoom::min, Step_Zoom::max);
    zoom_step_value.setDecimals(Step_Zoom::precision);
    zoom_step_value.setButtonSymbols(QAbstractSpinBox::NoButtons);
    zoom_step_value.setValue(zoom_step_placeholder_value);
    zoom_step_value.setMinimumWidth(step_zoom_minimum_width);

    zoom_min_value.setObjectName(zoom_min_name);
    zoom_max_value.setObjectName(zoom_max_name);
    zoom_step_value.setObjectName(zoom_step_name);

    zoom_box.setTitle(zoom);

    layer_2.addWidget(&zoom_min_title);
    layer_2.addWidget(&zoom_min_value);
    layer_2.addWidget(&zoom_max_title);
    layer_2.addWidget(&zoom_max_value);
    layer_2.addWidget(&zoom_step_title);
    layer_2.addWidget(&zoom_step_value);

    zoom_box.setLayout(&layer_2);
}

void Video_Tab::init_point()
{
    using namespace Video_Tab_Constant;

    QString point(point_groupbox_title);
    QString x(x_coordonate_title), y(y_coordonate_title);
    QString x_name(x_coordonate_name), y_name(y_coordonate_name);

    x_title.setText(x);
    y_title.setText(y);
    x_value.setObjectName(x_name);
    y_value.setObjectName(y_name);

    x_value.setRange(static_cast<int>(X_Point_Validator::min), static_cast<int>(X_Point_Validator::max));
    x_value.setDecimals(static_cast<int>(X_Point_Validator::precision));
    x_value.setButtonSymbols(QAbstractSpinBox::NoButtons);
    x_value.setValue(x_placeholder_value);
    x_value.setMinimumWidth(x_minimum_width);

    y_value.setRange(static_cast<int>(Y_Point_Validator::min), static_cast<int>(Y_Point_Validator::max));
    y_value.setDecimals(static_cast<int>(Y_Point_Validator::precision));
    y_value.setButtonSymbols(QAbstractSpinBox::NoButtons);
    y_value.setValue(y_placeholder_value);
    y_value.setMinimumWidth(y_minimum_width);

    layer_3.addWidget(&x_title);
    layer_3.addWidget(&x_value);
    layer_3.addWidget(&y_title);
    layer_3.addWidget(&y_value);

    point_box.setTitle(point);
    point_box.setLayout(&layer_3);
}


void Video_Tab::init_button()
{
    using namespace Video_Tab_Constant;

    QString create_and_save(create_and_save_groupbox_title);
    QString create_title(create_video_button_title);
    QString create_name(create_video_button_name);
    QString save_title(save_video_button_title);
    QString save_name(save_video_button_name);

    create_button.setText(create_title);
    save_button.setText(save_title);

    create_button.setObjectName(create_name);
    save_button.setObjectName(save_name);

    layer_4.addWidget(&create_button);
    layer_4.addWidget(&save_button);

    create_box.setTitle(create_and_save);
    create_box.setLayout(&layer_4);

    QObject::connect(&save_button, &QPushButton::clicked, this, &Video_Tab::open_file_explorer);
}

void Video_Tab::open_file_explorer()
{
  using namespace Video_Tab_Constant;

  QString fileName = QFileDialog::getSaveFileName(this, tr(save_window_title), default_video_name);

  if(!fileName.isEmpty())
  {
    emit file_saved(fileName);
  }
}
