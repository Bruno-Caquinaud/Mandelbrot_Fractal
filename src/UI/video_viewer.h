#ifndef VIDEO_VIEWER_H
#define VIDEO_VIEWER_H

#include <QObject>
#include <QWidget>
#include <QResizeEvent>
#include <QtMultimedia/QMediaPlayer>
#include <QtMultimediaWidgets/QVideoWidget>
#include <QVideoSurfaceFormat>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QPushButton>
#include <QUrl>
#include <QSize>
#include <QLabel>
#include "constant_ui.hpp"

class Video_Viewer : public QWidget
{
    Q_OBJECT

private:

    QVBoxLayout layer_1;
    QHBoxLayout layer_2;
    QHBoxLayout layer_3;
    QLabel spacer_1;
    QPushButton play_button;
    QPushButton pause_button;
    QPushButton stop_button;
    QVideoWidget video_widget;
    QMediaPlayer video_player;

public:

    Video_Viewer(QWidget * parent = nullptr);
    virtual ~Video_Viewer();
    void init_video_widget();
    void init_video_player();
    void init_button();
    void init_layout();

public slots :

    void set_media(QString & path);
    void restart();
};

#endif 
