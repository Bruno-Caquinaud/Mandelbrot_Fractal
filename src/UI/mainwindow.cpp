#include "mainwindow.h"

MainWindow::MainWindow(QWidget *parent): QMainWindow(parent),
      styleSheet_file(), size(),central_widget(this),
      layer_1(), layer_2(), layer_3(), onglets(&central_widget),
      image_tab(&onglets), video_tab(&onglets), viewer1(&image_tab), viewer2(&video_tab)
{
    init_size();
    init_tabs();
    init_viewer();
    init_layout();
    init_stylesheet();
}

void MainWindow::init_size()
{
    QDesktopWidget desktop;
    size = desktop.availableGeometry(this);
    this->resize(QSize(size.width(), size.height()));
}

void MainWindow::init_viewer()
{
    using namespace Tab_Constant;

    QString video_path(default_video_path);
    bool visible_viewer1 = default_visibility_image_viewer;
    bool visible_viewer2 = default_visibility_video_viewer;

    viewer1.setPixmap(QPixmap(default_image_path));
    viewer1.setVisible(visible_viewer1);
    viewer2.set_media(video_path);
    viewer2.setVisible(visible_viewer2);
}

void MainWindow::init_tabs()
{
    using namespace Tab_Constant;

    QString title_tab_image(image_tab_title);
    QString title_tab_video(video_tab_title);

    onglets.addTab(&image_tab, title_tab_image);
    onglets.addTab(&video_tab, title_tab_video);
    connect(&onglets, SIGNAL(tabBarClicked(int)), this, SLOT(switch_viewer(int)));
}

void MainWindow::init_layout()
{
    using namespace Window_Constant;

    layer_2.addWidget(&onglets);
    layer_3.addWidget(&viewer1);
    layer_3.addWidget(&viewer2);
    layer_1.addLayout(&layer_2, static_cast<int>(Layout_Space::Tab_Layout));
    layer_1.addLayout(&layer_3, static_cast<int>(Layout_Space::Viewer_Layout));
    central_widget.setLayout(&layer_1);
    this->setCentralWidget(&central_widget);
}

void MainWindow::init_stylesheet()
{
    using namespace Window_Constant;

    QString path(default_stylesheet_path);
    QString stylesheet_str(default_stylesheet_path);

    styleSheet_file.setFileName(path);
    styleSheet_file.open(QFile::ReadOnly);
    stylesheet_str = QLatin1String(styleSheet_file.readAll());
    this->setStyleSheet(stylesheet_str);
}

void MainWindow::switch_viewer(int view)
{
    using namespace Tab_Constant;

    bool visible_viewer1 = visibility_off;
    bool visible_viewer2 = visibility_off;

    switch (view)
    {
        case static_cast<int>(View_Selected::Image):
            visible_viewer1 = visibility_on;
            visible_viewer2 = visibility_off;
        break;

        case static_cast<int>(View_Selected::Video):
            visible_viewer1 = visibility_off;
            visible_viewer2 = visibility_on;
        break;
    default:
        break;
    }

    viewer1.setVisible(visible_viewer1);
    viewer2.setVisible(visible_viewer2);
}

MainWindow::~MainWindow()
{
}
