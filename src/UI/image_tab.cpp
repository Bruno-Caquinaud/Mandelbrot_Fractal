#include "image_tab.h"

Image_Tab::Image_Tab(QWidget *parent) : QWidget(parent),
    zoom_box(), point_box(), create_and_save_box(),
    layer_1(), layer_2(), layer_3(), layer_4(),
    zoom_title(), point_title(), x_title(), y_title(),
    create_title(), save_title(),
    zoom_value(), x_value(), y_value(),
    create_button(), save_button(),
    file_explorer()
{
    init_zoom();
    init_point();
    init_button();
    init_layer();
}

Image_Tab::~Image_Tab()
{

}

void Image_Tab::init_layer()
{
    using namespace Image_Tab_Constant;

    layer_1.addWidget(&zoom_box);
    layer_1.addWidget(&point_box);
    layer_1.addWidget(&create_and_save_box);
    this->setLayout(&layer_1);
    this->setObjectName(image_tab_name);
}

void Image_Tab::init_zoom()
{
    using namespace Image_Tab_Constant;

    QString zoom(zoom_groupbox_title);
    QString zoom_name(zoom_value_name);

    zoom_value.setObjectName(zoom_name);
    zoom_value.setRange(min_zoom_validator, max_zoom_validator);
    zoom_value.setDecimals(precision_zoom_validator);
    zoom_value.setButtonSymbols(QAbstractSpinBox::NoButtons);
    zoom_value.setValue(zoom_placeholder_value);
    zoom_value.setMinimumWidth(zoom_minimum_width);

    layer_2.addWidget(&zoom_value);

    zoom_box.setTitle(zoom);
    zoom_box.setLayout(&layer_2);
}

void Image_Tab::init_point()
{
    using namespace Image_Tab_Constant;

    QString point(point_groupbox_title);
    QString x(x_coordonate_title), y(y_coordonate_title);
    QString x_name(x_coordonate_name), y_name(y_coordonate_name);

    x_title.setText(x);
    y_title.setText(y);

    x_value.setObjectName(x_name);
    y_value.setObjectName(y_name);

    x_value.setRange(static_cast<int>(X_Point_Validator::min), static_cast<int>(X_Point_Validator::max));
    x_value.setDecimals(static_cast<int>(X_Point_Validator::precision));
    x_value.setButtonSymbols(QAbstractSpinBox::NoButtons);
    x_value.setValue(x_placeholder_value);
    x_value.setMinimumWidth(x_minimum_width);

    y_value.setRange(static_cast<int>(Y_Point_Validator::min), static_cast<int>(Y_Point_Validator::max));
    y_value.setDecimals(static_cast<int>(Y_Point_Validator::precision));
    y_value.setButtonSymbols(QAbstractSpinBox::NoButtons);
    y_value.setValue(y_placeholder_value);
    y_value.setMinimumWidth(y_minimum_width);

    layer_3.addWidget(&x_title);
    layer_3.addWidget(&x_value);
    layer_3.addWidget(&y_title);
    layer_3.addWidget(&y_value);

    point_box.setTitle(point);
    point_box.setLayout(&layer_3);
}

void Image_Tab::init_button()
{
    using namespace Image_Tab_Constant;

    QString create_and_save(create_and_save_groupbox_title);
    QString create_title(create_image_button_title);
    QString create_name(create_image_button_name);
    QString save_title(save_image_button_title);
    QString save_name(save_image_button_name);

    create_button.setText(create_title);
    save_button.setText(save_title);
    create_button.setObjectName(create_name);
    save_button.setObjectName(save_name);

    layer_4.addWidget(&create_button);
    layer_4.addWidget(&save_button);

    create_and_save_box.setTitle(create_and_save);
    create_and_save_box.setLayout(&layer_4);

    QObject::connect(&save_button, &QPushButton::clicked, this, &Image_Tab::open_file_explorer);
}

void Image_Tab::open_file_explorer()
{
  using namespace Image_Tab_Constant;

  QString fileName = QFileDialog::getSaveFileName(this, tr(save_window_title), default_image_name);

  if(!fileName.isEmpty())
  {
    int position_extension = fileName.indexOf(default_caracter_extension);

    if(position_extension > indexOf_error)
    {
      fileName.remove(position_extension, fileName.length());
    }

    fileName.append(default_extension_file);

    emit file_saved(fileName);
  }
}
