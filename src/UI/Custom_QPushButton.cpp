#include "Custom_QPushButton.hpp"

using namespace Image_Tab_Constant;

Custom_QPushButton::Custom_QPushButton(QWidget * parent) : QPushButton(parent), state(enabled_button_state)
{
  QObject::connect(this, &QPushButton::clicked, this, &Custom_QPushButton::change_state);
  QObject::connect(this, &Custom_QPushButton::launch_action, this, &Custom_QPushButton::change_text);
  QObject::connect(this, &Custom_QPushButton::cancel_action, this, &Custom_QPushButton::change_text);
}

Custom_QPushButton::~Custom_QPushButton()
{

}

void Custom_QPushButton::change_state()
{
  if(state)
  {
    state = disabled_button_state;
    emit launch_action();
  }
  else
  {
    state = enabled_button_state;
    emit cancel_action();
  }
}

void Custom_QPushButton::change_text()
{
  if(!state)
  {
    setText(cancel_image_button_title);
  }
  else
  {
    setText(create_image_button_title);
  }
}

void Custom_QPushButton::set_state(bool value)
{
  state = value;
}
