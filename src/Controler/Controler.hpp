#ifndef CONTROLER_HEADER_HPP
#define CONTROLER_HEADER_HPP

#include "../UI/mainwindow.h"
#include "../Backend/Mandelbrot_Video.hpp"

class Controler : public QObject
{
  Q_OBJECT

  private :

  Mandelbrot_Image<double> * image;
  Mandelbrot_Video<double> * video;
  MainWindow * mandelbrot_ui;
  unique_ptr<std::jthread> image_thread;
  unique_ptr<std::jthread> video_thread;
  std::stop_source stop_source_image;
  std::stop_source stop_source_video;

  void link_view_and_model();
  void link_view();
  void link_model();
  void link_image();
  void link_video();

  public :

  Controler(Mandelbrot_Image<double> * image, Mandelbrot_Video<double> * video, MainWindow * view);

  signals:

  void start_display_image(const QPixmap &);
  void start_save_image(QString & );
  void start_display_video(QString &);
  void start_save_video(QString &);

  public slots:

  void launch_image_creation();
  void cancel_image_creation();
  void launch_save_image(QString &);
  void launch_video_creation();
  void cancel_video_creation();
  void launch_save_video(QString &);
  void launch_image_displaying();
  void launch_video_displaying(const char *);
};

#endif
