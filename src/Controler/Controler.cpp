#include "Controler.hpp"

Controler::Controler(Mandelbrot_Image<double> * image, Mandelbrot_Video<double> * video, MainWindow * view):
image(image), video(video), mandelbrot_ui(view),
image_thread(), video_thread(),
stop_source_image(), stop_source_video()
{
  link_view_and_model();
}

void Controler::link_view_and_model()
{
  link_view();
  link_model();
}

void Controler::link_view()
{
  link_image();
  link_video();
}

void Controler::link_image()
{
  using namespace Image_Tab_Constant;
  using namespace Image_Viewer_Constant;

  QWidget * central_widget = mandelbrot_ui->centralWidget();
  Custom_QPushButton * create_image = central_widget->findChild<Custom_QPushButton *>(create_image_button_name);
  Image_Viewer * image_viewer = central_widget->findChild<Image_Viewer *>(image_viewer_name);
  Image_Tab * image_tab = central_widget->findChild<Image_Tab *>(image_tab_name);

  QObject::connect(create_image, &Custom_QPushButton::launch_action, this, &Controler::launch_image_creation);
  QObject::connect(create_image, &Custom_QPushButton::cancel_action, this, &Controler::cancel_image_creation);
  QObject::connect(this, &Controler::start_display_image, image_viewer, &Image_Viewer::setPixmap);
  QObject::connect(image_tab, &Image_Tab::file_saved, this, &Controler::launch_save_image);
}

void Controler::link_video()
{
  using namespace Video_Tab_Constant;
  using namespace Video_Viewer_Constant;

  QWidget * central_widget = mandelbrot_ui->centralWidget();

  Custom_QPushButton * create_video = central_widget->findChild<Custom_QPushButton *>(create_video_button_name);
  Video_Viewer * video_viewer = central_widget->findChild<Video_Viewer *>(video_viewer_name);
  Video_Tab * video_tab = central_widget->findChild<Video_Tab *>(video_tab_name);

  QObject::connect(create_video, &Custom_QPushButton::launch_action, this, &Controler::launch_video_creation);
  QObject::connect(create_video, &Custom_QPushButton::cancel_action, this, &Controler::cancel_video_creation);
  QObject::connect(this, &Controler::start_display_video, video_viewer, &Video_Viewer::set_media);
  QObject::connect(video_tab, &Video_Tab::file_saved, this, &Controler::launch_save_video);
}


void Controler::launch_image_creation()
{
  using namespace Image_Tab_Constant;

  QWidget * central_widget = mandelbrot_ui->centralWidget();

  Custom_QDoubleSpinBox * zoom_image = central_widget->findChild<Custom_QDoubleSpinBox *>(zoom_value_name);
  Custom_QDoubleSpinBox * x_position = central_widget->findChild<Custom_QDoubleSpinBox *>(x_coordonate_name);
  Custom_QDoubleSpinBox * y_position = central_widget->findChild<Custom_QDoubleSpinBox *>(y_coordonate_name);

  double zoom = zoom_image->value();
  double x = x_position->value();
  double y = y_position->value();

  image->set_zoom(static_cast<double>(zoom));
  image->set_center_point(complex<double>(static_cast<double>(x), static_cast<double>(y)));

  image_thread = make_unique<std::jthread>(&Mandelbrot_Image<double>::create, image, stop_source_image.get_token());
}

void Controler::launch_image_displaying()
{
  using namespace Image_Tab_Constant;

  QWidget * central_widget = mandelbrot_ui->centralWidget();
  Custom_QPushButton * create_image = central_widget->findChild<Custom_QPushButton *>(create_image_button_name);
  Mat image_mat(image->get_image());

  QImage image_qt = QImage((uchar*) image_mat.data, image_mat.cols, image_mat.rows, image_mat.step, QImage::Format_RGB888);
  QPixmap image_pix;

  image_qt = image_qt.rgbSwapped();
  image_pix = QPixmap::fromImage(image_qt);

  create_image->set_state(bool(enabled_button_state));
  create_image->change_text();
  emit start_display_image(image_pix);
}

void Controler::cancel_image_creation()
{
  stop_source_image.request_stop();
  stop_source_image = stop_source();
}

void Controler::launch_save_image(QString & filename)
{
    emit start_save_image(filename);
}

void Controler::launch_video_creation()
{
  using namespace Video_Tab_Constant;

  QWidget * central_widget = mandelbrot_ui->centralWidget();
  Custom_QDoubleSpinBox * zoom_min_video = central_widget->findChild<Custom_QDoubleSpinBox *>(zoom_min_value_name);
  Custom_QDoubleSpinBox * zoom_max_video = central_widget->findChild<Custom_QDoubleSpinBox *>(zoom_max_value_name);
  Custom_QDoubleSpinBox * zoom_step_video = central_widget->findChild<Custom_QDoubleSpinBox *>(zoom_step_value_name);

  Custom_QDoubleSpinBox * x_position = central_widget->findChild<Custom_QDoubleSpinBox *>(x_coordonate_name);
  Custom_QDoubleSpinBox * y_position = central_widget->findChild<Custom_QDoubleSpinBox *>(y_coordonate_name);

  double zoom_min = zoom_min_video->value();
  double zoom_max = zoom_max_video->value();
  double zoom_step = zoom_step_video->value();
  double x = x_position->value();
  double y = y_position->value();

  video->set_zoom(static_cast<double>(zoom_min));
  video->set_zoom_max(static_cast<double>(zoom_max));
  video->set_zoom_step(static_cast<double>(zoom_step));
  video->set_center_point(complex<double>(static_cast<double>(x), static_cast<double>(y)));

  video_thread = make_unique<std::jthread>(&Mandelbrot_Video<double>::create, video, stop_source_video.get_token());
}

void Controler::cancel_video_creation()
{
  stop_source_video.request_stop();
  stop_source_video = stop_source();
}

void Controler::launch_video_displaying(const char * filename)
{
  using namespace std;
  using namespace Image_Tab_Constant;
  using namespace Video_Tab_Constant;

  QWidget * central_widget = mandelbrot_ui->centralWidget();
  Custom_QPushButton * create_video = central_widget->findChild<Custom_QPushButton *>(create_video_button_name);
  string absolute_current_dir_path(filesystem::current_path());
  QString video_path;

  video_path.append(absolute_current_dir_path.data());
  video_path.append('/');
  video_path.append(filename);

  create_video->set_state(bool(enabled_button_state));
  create_video->change_text();
  emit start_display_video(video_path);
}

void Controler::launch_save_video(QString & filename)
{
    emit start_save_video(filename);
}

void Controler::link_model()
{
  QObject::connect(image, &Mandelbrot_Image<double>::image_creation_finished, this, &Controler::launch_image_displaying);
  QObject::connect(this, &Controler::start_save_image, image, &Mandelbrot_Image<double>::save_image);

  QObject::connect(video, &Mandelbrot_Video<double>::video_creation_finished, this, &Controler::launch_video_displaying);
  QObject::connect(this, &Controler::start_save_video, video, &Mandelbrot_Video<double>::save_video);
}
