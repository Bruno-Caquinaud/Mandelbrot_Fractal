#ifndef MANDELBROT_IMAGE_SOURCE_CPP
#define MANDELBROT_IMAGE_SOURCE_CPP
#include "Mandelbrot_Image.hpp"

using namespace Mandelbrot_Image_Constant;

template<typename T>
Mandelbrot_Image<T>::Mandelbrot_Image():
concurrent_thread_number(default_concurrent_thread_number),
height(default_height), width(default_width),
center_point(default_center_point_x, default_center_point_y),
zoom(default_zoom1), offset(default_offset),
x_min(default_xmin), y_max(default_ymax),
x_step(default_x_step), y_step(default_y_step),
image(), lut(default_lut_size, gradient_color)
{
  thread_setting();
  configure();
}

template<typename T>
void Mandelbrot_Image<T>::thread_setting()
{
  unsigned int concurrent_thread_detected = std::thread::hardware_concurrency();

  if(concurrent_thread_number < concurrent_thread_detected)
  {
    concurrent_thread_number = concurrent_thread_detected;
  }
}

template<typename T>
void Mandelbrot_Image<T>::configure()
{
  x_min = center_point.real() - offset / zoom;
  y_max = center_point.imag() + offset / zoom;

  assert(height > min_height && width > min_width);

  x_step = (x_step_coeff * offset / zoom ) / height;
  y_step = (y_step_coeff * offset / zoom ) / width;

  image = cv::Mat::zeros(height, width, channel_type);
}

template<typename T>
void Mandelbrot_Image<T>::set_height(T height)
{
  this->height = height;
  configure();
}

template<typename T>
void Mandelbrot_Image<T>::set_width(T width)
{
  this->width = width;
  configure();
}

template<typename T>
void Mandelbrot_Image<T>::set_zoom(T zoom)
{
  this->zoom = zoom;
  configure();
}

template<typename T>
T Mandelbrot_Image<T>::get_zoom()
{
  return zoom;
}

template<typename T>
void Mandelbrot_Image<T>::set_center_point(complex<T> center_point)
{
  this->center_point = center_point;
  configure();
}

template<typename T>
void Mandelbrot_Image<T>::set_offset(T offset)
{
  this->offset = offset;
  configure();
}

template<typename T>
cv::Mat Mandelbrot_Image<T>::get_image()
{
  return image;
}

template<class T>
void Mandelbrot_Image<T>::set_lut(Look_Up_Table & lut)
{
  this->lut = lut;
}

template<class T>
void Mandelbrot_Image<T>::create(std::stop_token stop_token)
{
  using namespace std;

  vector<Mandelbrot_Point<T>> mandelbrot_points_image(height * width);
  auto start2 = chrono::high_resolution_clock::now();
  vector<jthread> threads;

  for(unsigned int i = 0; i < concurrent_thread_number && !stop_token.stop_requested(); i++)
  {
    threads.emplace_back(&Mandelbrot_Image<T>::calcul_image_mandelbrot_point, this, &mandelbrot_points_image, i, concurrent_thread_number, stop_token);
  }

  for(jthread & th : threads)
  {
    if(th.joinable())
    {
      th.join();
    }
  }

  if(!stop_token.stop_requested())
  {
    convert_mandelbrot_points_to_pixels_image(mandelbrot_points_image, image);

    auto start3 = std::chrono::high_resolution_clock::now();
    auto elapsed2 = std::chrono::duration_cast<std::chrono::milliseconds>(start3 - start2);
    cerr << "Ellapsed : [ " << elapsed2.count() << " ]"<< endl;

    emit image_creation_finished();
  }
}

template<typename T>
void Mandelbrot_Image<T>::calcul_image_mandelbrot_point(std::vector<Mandelbrot_Point<T>> * mandelbrot_points_image, unsigned int step_number, unsigned int thread_number, std::stop_token stop_token)
{
  T x, y;
  unsigned int start_height, end_height, start_index;

  start_height = (height / thread_number) * step_number;
  end_height = (height / thread_number) * (step_number + 1);
  start_index = (height / thread_number) * step_number * width;
  y = y_max - (start_height * y_step);

  if(step_number == (thread_number - 1))
  {
    end_height = height;
  }

  for(unsigned int i = start_height, index = start_index; i < end_height && !stop_token.stop_requested(); i++)
  {
    x = x_min;

    for(unsigned int j = starting_col_image; j < width && !stop_token.stop_requested(); j++, index++)
    {
      Mandelbrot_Point<T> & current_point = mandelbrot_points_image->at(index);

      current_point.set_point(std::complex<T>(x, y));
      current_point.is_mandelbrot_set();
      x += x_step;
    }

    y -= y_step;
  }
}

template<class T>
void Mandelbrot_Image<T>::convert_mandelbrot_points_to_pixels_image(vector<Mandelbrot_Point<T>> & mandelbrot_points_image, cv::Mat & image)
{
  if(mandelbrot_points_image.size() == static_cast<unsigned int>(image.rows * image.cols))
  {
    for(int i = starting_raw_image, index = starting_point_index; i < image.rows; i++)
    {
      for(int j = starting_col_image; j < image.cols; j++, index++)
      {
        lut.look_up(mandelbrot_points_image.at(index).get_iteration_number(), image.at<Vec3b>(i, j));
      }
    }
  }
}

template<class T>
void Mandelbrot_Image<T>::save_image(QString & filename)
{
  cv::imwrite(filename.toStdString(), image);
}

/*
template<typename T>
void Mandelbrot_App<T>::calcul_image_mandelbrot_point_optimization()
{
  for(int i = 0; i < height; i++)
  {
    for(int j = 0; j < width; j+=4)
    {
      vector<T> x(4), y(4);
      vector<int> nombre_iteration(4, 0);
      vector<__m256d> point(2);

      for(int k = 0; k < 4; k+=2)
      {
        x.at(k) = x_min + (j + k) * x_step;
        y.at(k) = y_max - i * y_step;
        x.at(k+1) = x_min + (j + k + 1) * x_step;
        y.at(k+1) = y_max - i * y_step;

        point.at(k/2) = _mm256_set_pd(y.at(k+1), x.at(k+1), y.at(k), x.at(k));
      }

      is_mandelbrot_set_optimization(point, nombre_iteration);

      for(int k = 0; k < 4; k++)
      {
        pair<complex<T>, int> couple(complex<T>(0,0), nombre_iteration.at(k));
        iterations.at( i * width + j + k) = couple;
      }
    }
  }
}
*/

#endif
