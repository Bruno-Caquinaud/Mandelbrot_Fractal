#ifndef VIDEO_HEADER_HPP
#define VIDEO_HEADER_HPP

#include <iostream>
#include <list>
#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/videoio.hpp>
#include "constant_backend.hpp"

using namespace std;
using namespace cv;
using namespace Video_Constant;

class Video
{
  friend class Video_Test;

  private :

  string filename;
  int codec;
  Size size;
  int frame_per_sec;
  list<Mat> images;
  VideoWriter generator;

  public :

  Video(int frame_per_sec_p = default_frame_per_sec, int codec = cv::VideoWriter::fourcc(default_codec_abreviation[0], default_codec_abreviation[1], default_codec_abreviation[2], default_codec_abreviation[3]), Size size = Size(default_video_width, default_video_height));
  ~Video();
  void play() const;
  list<Mat> & get_images_list();
  void build();
};

#endif
