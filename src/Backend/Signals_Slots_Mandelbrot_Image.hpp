#ifndef SIGNALS_SLOTS_MANDELBROT_Image_HEADER_HPP
#define SIGNALS_SLOTS_MANDELBROT_Image_HEADER_HPP

#include <QObject>
#include <QString>

class Signals_Slots_Mandelbrot_Image : public QObject
{
  Q_OBJECT

  signals :
  void image_creation_finished();

  public slots:
  virtual void save_image(QString &) = 0;
};

#endif
