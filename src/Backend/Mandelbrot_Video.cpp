#ifndef MANDELBROT_VIDEO_SOURCE_CPP
#define MANDELBROT_VIDEO_SOURCE_CPP
#include "Mandelbrot_Video.hpp"

using namespace Mandelbrot_Video_Constant;

template<class T>
Mandelbrot_Video<T>::Mandelbrot_Video():
image(),
zoom_max(default_zoom2), zoom_step(default_step),
video()
{

}

template<typename T>
void Mandelbrot_Video<T>::set_zoom(T zoom)
{
  image.set_zoom(zoom);
}

template<typename T>
void Mandelbrot_Video<T>::set_center_point(std::complex<T> center_point)
{
  image.set_center_point(center_point);
}

template<typename T>
void Mandelbrot_Video<T>::set_zoom_max(T zoom)
{
  this->zoom_max = zoom;
}

template<typename T>
void Mandelbrot_Video<T>::set_zoom_step(T zoom)
{
  this->zoom_step = zoom;
}

template<class T>
void Mandelbrot_Video<T>::create(std::stop_token stop_token)
{
    if(image.get_zoom() > default_min_zoom1 && zoom_max > default_min_zoom2
      && zoom_step > default_min_step && zoom_step < default_max_step)
    {
      T base_zoom, local_step_zoom;

      base_zoom = image.get_zoom();
      local_step_zoom = (zoom_max - base_zoom) / zoom_step;
      video.get_images_list().clear();

      for(int i = default_min_step; i < zoom_step && !stop_token.stop_requested(); i++)
      {
        image.create(stop_token);
        video.get_images_list().push_back(image.get_image().clone());
        image.set_zoom(base_zoom + local_step_zoom * i);
      }

      if(!stop_token.stop_requested())
      {
        video.build();
      }
    }

    emit video_creation_finished(default_tmp_video_filename);
}

template<class T>
void Mandelbrot_Video<T>::save_video(QString & filename)
{
  using namespace Video_Constant;
  std::filesystem::copy_file(default_tmp_video_filename, filename.toStdString());
}

#endif
