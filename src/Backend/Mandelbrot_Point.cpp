#ifndef MANDELBROT_POINT_SOURCE_CPP
#define MANDELBROT_POINT_SOURCE_CPP
#include "Mandelbrot_Point.hpp"

template<class T>
Mandelbrot_Point<T>::Mandelbrot_Point(std::complex<T> point, unsigned int max_iteration, unsigned int max_absolute):
point(point), suit_value(default_real_mandelbrot_suit, default_imaginary_mandelbrot_suit), iteration_number(default_iteration_number),
max_iteration_number(max_iteration), max_absolute_value(max_absolute)
{

}

template<class T>
void Mandelbrot_Point<T>::set_point(std::complex<T> & point)
{
  this->point = point;
}

template<class T>
void Mandelbrot_Point<T>::set_point(std::complex<T> && point)
{
  this->point = point;
}

template<class T>
unsigned int Mandelbrot_Point<T>::get_iteration_number()
{
  return iteration_number;
}

template<class T>
void Mandelbrot_Point<T>::mandelbrot_algorithm()
{
  suit_value *= suit_value;
  suit_value += point;
}

template<class T>
bool Mandelbrot_Point<T>::is_mandelbrot_set()
{
  using namespace std;

  bool is_converge = unconvergent_suit;
  T absolu = default_absolu_point_value;

  suit_value = complex<T>(default_real_mandelbrot_suit, default_imaginary_mandelbrot_suit);
  iteration_number = default_iteration_number;

  while(absolu < max_absolute_value && iteration_number < max_iteration_number)
  {
    mandelbrot_algorithm();
    iteration_number++;
    absolu = pow(suit_value.real(), square_pow) + pow(suit_value.imag(), square_pow);
  }

  if(iteration_number == max_iteration_number)
  {
    is_converge = convergent_suit;
  }

  return is_converge;
}

/*template<class T>
vector<bool> Mandelbrot_Point<T>::is_mandelbrot_set_optimization(vector<__m256d> & point, vector<int> & nombre_iteration, int max_iteration)
{
  vector<bool> bits(4, 1);
  vector<__m256d> mandelbrot_suit(4);
  vector<__m256d> absolu(2), absolu_ref(1);

  for(__m256d & num : mandelbrot_suit)
  {
    num = _mm256_setzero_pd();
  }

  for(__m256d & num : absolu_ref)
  {
    num = _mm256_set1_pd(4.0);
  }

  while(bits.at(0) != 0
      || bits.at(1) != 0
      || bits.at(2) != 0
      || bits.at(3) != 0)
  {
    // VEC 1 : a b c d          VEC 3 : e f g h
    // VEC 2 : a b c d          VEC 4 : e f g h
    // VEC 1 = VEC 1 * VEC 2    VEC 3 = VEC 3 * VEC 4
    //       = a² b² c² d²            = e² f² g² h²
    // VEC 1 = SUB_h(VEC 1) , SUB_h(VEC 3)
    //       = a²-b² c²-d² e²-f² g²-h²
    // VEC 3 = the two last element of VEC1
    //       = e²-f² f g²-h² h
    // VEC 1 = switch 2 et 3
    // VEC 1 = a²-b² e²-f² c²-d² g²-h²

    // VEC 5 = a c e g  VEC 6 = b d f h VEC 7 = 2 2 2 2
    // VEC 5 = VEC 5 * VEC 6 * VEC 7
    //       = 2*a*b 2*c*d 2*e*f 2*g*h

    // VEC 1 = a²-b² 2*a*b c²-d² 2*c*d
    // VEC 3 = e²-f² 2*e*f g²-h² 2*g*h

    // VEC abs

    vector<__m256d> is_converge (1);

    mandelbrot_suit.at(2) = _mm256_shuffle_pd(mandelbrot_suit.at(0), mandelbrot_suit.at(1), 0b0000);
    mandelbrot_suit.at(3) = _mm256_shuffle_pd(mandelbrot_suit.at(0), mandelbrot_suit.at(1), 0b1111);
    mandelbrot_suit.at(2) = _mm256_permute4x64_pd(mandelbrot_suit.at(2), 0b11011000);
    mandelbrot_suit.at(3) = _mm256_permute4x64_pd(mandelbrot_suit.at(3), 0b11011000);

    mandelbrot_suit.at(0) = _mm256_mul_pd(mandelbrot_suit.at(0), mandelbrot_suit.at(0));
    mandelbrot_suit.at(1) = _mm256_mul_pd(mandelbrot_suit.at(1), mandelbrot_suit.at(1));
    mandelbrot_suit.at(0) = _mm256_hsub_pd(mandelbrot_suit.at(0), mandelbrot_suit.at(1));

    mandelbrot_suit.at(2) = _mm256_mul_pd(mandelbrot_suit.at(2), mandelbrot_suit.at(3));
    mandelbrot_suit.at(2) = _mm256_add_pd(mandelbrot_suit.at(2), mandelbrot_suit.at(2));
    mandelbrot_suit.at(2) = _mm256_permute4x64_pd(mandelbrot_suit.at(2), 0b11011000);

    mandelbrot_suit.at(1) = _mm256_shuffle_pd(mandelbrot_suit.at(0), mandelbrot_suit.at(2), 0b1111);
    mandelbrot_suit.at(0) = _mm256_shuffle_pd(mandelbrot_suit.at(0), mandelbrot_suit.at(2), 0b0000);

    mandelbrot_suit.at(0) = _mm256_add_pd(mandelbrot_suit.at(0), point.at(0));
    mandelbrot_suit.at(1) = _mm256_add_pd(mandelbrot_suit.at(1), point.at(1));

    absolu.at(0) = _mm256_mul_pd(mandelbrot_suit.at(0), mandelbrot_suit.at(0));
    absolu.at(1) = _mm256_mul_pd(mandelbrot_suit.at(1), mandelbrot_suit.at(1));
    absolu.at(0) = _mm256_hadd_pd(absolu.at(0), absolu.at(1));
    is_converge.at(0) = _mm256_cmp_pd(absolu.at(0), absolu_ref.at(0), 0x12);

    for(__m256d & converge : is_converge)
    {
      const size_t n = sizeof(__m256d) / sizeof(T);
      T buffer[n] = {0};
      _mm256_storeu_pd(buffer, converge);

      for(int i = 0; i < 4; i++)
      {
        bits.at(i) = isnan(buffer[i]);

        switch(bits.at(i))
        {
          case true:
          if(nombre_iteration.at(i) != max_iteration)
          {
            nombre_iteration.at(i)++;
          }
          else
          {
            bits.at(i) = false;
          }
          break;
          default:
          break;
        }
      }
    }
  }
  return bits;
}*/

#endif
