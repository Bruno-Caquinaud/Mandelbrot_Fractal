#ifndef SIGNALS_SLOTS_MANDELBROT__VIDEO_HEADER_HPP
#define SIGNALS_SLOTS_MANDELBROT_VIDEO_HEADER_HPP

#include <QObject>
#include <QString>

class Signals_Slots_Mandelbrot_Video : public QObject
{
  Q_OBJECT

  signals :
  void video_creation_finished(const char * filename);

  public slots:
  virtual void save_video(QString &) = 0;
};

#endif
