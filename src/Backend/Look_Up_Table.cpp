#include "Look_Up_Table.hpp"

Look_Up_Table::Look_Up_Table(unsigned int size, function<void(Vec3b &, int, vector<pair<Vec3b, int>> &)> function):
table(size),  colors(default_colors), look_up_function(function)
{
  init();
}

void Look_Up_Table::init()
{
  if(look_up_function)
  {
    for(unsigned int i = starting_index_table; i < table.size(); i++)
    {
      look_up_function(table.at(i), i, colors);
    }
  }
}

void Look_Up_Table::look_up(unsigned int input, Vec3b & output)
{
  output = table.at(input);
}

void Look_Up_Table::add_color(pair<Vec3b, int> & color)
{
  colors.push_back(color);
}

void Look_Up_Table::remove_color()
{
  colors.erase(colors.begin());
}

void gradient_color(Vec3b & element, int index, vector<pair<Vec3b, int>> & colors_table)
{
  bool percentage_point_state = percentage_point_valid;

  for(pair<Vec3b, int> point : colors_table)
  {
      if(!(point.second >= min_percentage_point_limit && point.second <= max_percentage_point_limit) && percentage_point_state)
      {
        percentage_point_state = percentage_point_wrong;
      }
  }

  if(percentage_point_state == percentage_point_valid)
  {
    float value;
    int index1_colors_table, index2_colors_table;

    value = (index * default_gradient_color_coefficient) % default_gradient_color_modulo;
    index1_colors_table = index2_colors_table = default_index_colors_position;

    for(unsigned int i = starting_index_table, j = colors_table.size() - 1; i < colors_table.size(); i++, j--)
    {
      if(value >= colors_table.at(i).second)
      {
        index1_colors_table = i;
      }
      if(value <= colors_table.at(j).second)
      {
        index2_colors_table = j;
      }
    }

    if(index >=threshold_index)
    {
      element = colors_table.at(index1_colors_table).first;

      for(int j = starting_index_Vec3b; j < size_Vec3b; j++)
      {
        element[j] += (colors_table.at(index2_colors_table).first[j] - element[j]) * ((float)(value/colors_table.at(index2_colors_table).second));
      }
    }
    else
    {
      element = colors_table.at(starting_index_table).first;
    }
  }
}
