#ifndef MANDELBROT_IMAGE_HEADER_HPP
#define MANDELBROT_IMAGE_HEADER_HPP

#include <thread>
#include <cassert>
#include <opencv2/imgcodecs.hpp>

#include "Look_Up_Table.hpp"
#include "Mandelbrot_Point.hpp"
#include "Signals_Slots_Mandelbrot_Image.hpp"

template<typename T>
class Mandelbrot_Image : public Signals_Slots_Mandelbrot_Image
{
  private :

  unsigned int concurrent_thread_number;
  unsigned int height, width;
  std::complex<T> center_point;
  T zoom, offset;
  T x_min, y_max;
  T x_step, y_step;

  cv::Mat image;
  Look_Up_Table lut;

  void thread_setting();
  void configure();

  public :

  Mandelbrot_Image();

  void set_height(T height);
  void set_width(T width);
  void set_center_point(std::complex<T> center_point);
  void set_offset(T offset);
  void set_zoom(T zoom);
  void set_lut(Look_Up_Table & lut);
  T get_zoom();

  Mat get_image();
  void create(std::stop_token stop_token);
  void save_image(QString &) override;
  void convert_mandelbrot_points_to_pixels_image(std::vector<Mandelbrot_Point<T>> & mandelbrot_points_image, cv::Mat & image);
  void calcul_image_mandelbrot_point(std::vector<Mandelbrot_Point<T>> * iterations, unsigned int thread_number, unsigned int thread_size, std::stop_token stop_token);
};

#ifndef MANDELBROT_IMAGE_SOURCE_CPP
#include "Mandelbrot_Image.cpp"
#endif

#endif
