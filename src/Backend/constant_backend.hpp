#ifndef CONSTANT_BACKEND_HEADER_H
#define CONSTANT_BACKEND_HEADER_H

#include <vector>
#include <utility>
#include "opencv2/core.hpp"

namespace Look_Up_Table_Constant
{
  using namespace std;
  using namespace cv;

  const vector<pair<Vec3b, int>> default_colors {
    pair<Vec3b, int>(Vec3b(0x2A, 0x2A, 0x2A), 0),
    pair<Vec3b, int>(Vec3b(0x30, 0x1E, 0x14), 10),
    pair<Vec3b, int>(Vec3b(0x55, 0x3B, 0x24), 20),
    pair<Vec3b, int>(Vec3b(0x95, 0x37, 0x36), 30),
    pair<Vec3b, int>(Vec3b(0x97, 0x5C, 0x00), 40),
    pair<Vec3b, int>(Vec3b(0x52, 0xd4, 0xff), 50),
    pair<Vec3b, int>(Vec3b(0x1a, 0x4a, 0xfc), 60),
    pair<Vec3b, int>(Vec3b(0x00, 0x00, 0xf0), 70),
    pair<Vec3b, int>(Vec3b(0x9c, 0x4e, 0xbc), 80),
    pair<Vec3b, int>(Vec3b(0x48, 0x00, 0x48), 90),
    pair<Vec3b, int>(Vec3b(0x00, 0x00, 0x00), 100)
  };

  constexpr const unsigned int starting_index_table = 0;
  constexpr const bool percentage_point_valid = true;
  constexpr const bool percentage_point_wrong = false;
  constexpr const int min_percentage_point_limit = 0;
  constexpr const int max_percentage_point_limit = 100;
  constexpr const int default_index_colors_position = 0;
  constexpr const int default_gradient_color_coefficient = 6;
  constexpr const int default_gradient_color_modulo = 101;
  constexpr const int starting_index_Vec3b = 0;
  constexpr const int size_Vec3b = 3;
  constexpr const int threshold_index = 7;
}

namespace Mandelbrot_Point_Constant
{
  constexpr const int max_iteration = 1024;
  constexpr const int absolute_limit_mandelbrot_suit = 4;
  constexpr const float default_real_mandelbrot_point = 0;
  constexpr const float default_imaginary_mandelbrot_point = 0;
  constexpr const float default_real_mandelbrot_suit = 0;
  constexpr const float default_imaginary_mandelbrot_suit = 0;
  constexpr const int default_iteration_number = 0;
  constexpr const bool convergent_suit = true;
  constexpr const bool unconvergent_suit = false;
  constexpr const float default_absolu_point_value = 0;
  constexpr const int square_pow = 2;
}

namespace Video_Constant
{
  constexpr const int default_frame_per_sec = 10;
  constexpr const char default_tmp_video_filename[] = "video_tmp0001.avi";
  constexpr const char default_codec_abreviation[] = "MJPG";
  constexpr const int default_video_height = 800;
  constexpr const int default_video_width = 800;
}

namespace Mandelbrot_Image_Constant
{
  constexpr const unsigned int default_concurrent_thread_number = 1;
  constexpr const int default_width = 800;
  constexpr const int default_height = 800;
  constexpr const float default_center_point_x = -1.24413083223632811121;
  constexpr const float default_center_point_y = 0.07254229559466145827;
  constexpr const float default_zoom1 = 1;
  constexpr const int default_offset = 2;
  constexpr const float default_xmin = 0;
  constexpr const float default_ymax = 0;
  constexpr const float default_x_step = 0;
  constexpr const float default_y_step = 0;
  constexpr const int default_lut_size = Mandelbrot_Point_Constant::max_iteration + 1;
  constexpr const int min_width = 0;
  constexpr const int min_height = 0;
  constexpr const float x_step_coeff = 2;
  constexpr const float y_step_coeff = 2;
  constexpr const int channel_type = CV_8UC3;
  constexpr const int starting_col_image = 0;
  constexpr const int starting_raw_image = 0;
  constexpr const int starting_point_index = 0;
}

namespace Mandelbrot_Video_Constant
{
  constexpr const float default_zoom2 = 2;
  constexpr const float default_step = 0.05;
  constexpr const float default_min_zoom1 = 0;
  constexpr const float default_min_zoom2 = 0;
  constexpr const float default_min_step = 1;
  constexpr const float default_max_step = 1000000;
}

#endif
