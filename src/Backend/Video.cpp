#include "Video.hpp"

Video::Video(int frame_per_sec_p, int codec_p, Size size_p):
filename(default_tmp_video_filename), codec(codec_p),
size(size_p),
frame_per_sec(frame_per_sec_p),
images(),
generator()
{

}

Video::~Video()
{

}

list<Mat> & Video::get_images_list()
{
  return images;
}

void Video::build()
{
  generator.open(filename, codec, frame_per_sec, size);

  if(generator.isOpened())
  {
    for(Mat & image : images)
    {
      cout << "Writing image" << endl;
      generator.write(image);
    }

    generator.release();
  }

}

void Video::play() const
{
  for(const Mat & image : images)
  {
    imshow("Display window", image);
    waitKey(frame_per_sec);
  }
}
