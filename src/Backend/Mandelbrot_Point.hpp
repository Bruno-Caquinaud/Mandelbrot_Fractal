#ifndef MANDELBROT_POINT_HEADER_HPP
#define MANDELBROT_POINT_HEADER_HPP

#include <iostream>
#include <complex>
#include <cmath>
#include <vector>
#include "constant_backend.hpp"

using namespace Mandelbrot_Point_Constant;

template< typename T>
class Mandelbrot_Point
{
  template<class U>
  friend class Mandelbrot_Point_Test;

  private:
  std::complex<T> point, suit_value;
  unsigned int iteration_number, max_iteration_number, max_absolute_value;

  public:

  Mandelbrot_Point(std::complex<T> point = std::complex<T>(default_real_mandelbrot_point, default_imaginary_mandelbrot_point), unsigned int maximum_iteration = max_iteration, unsigned int max_absolute = absolute_limit_mandelbrot_suit);

  void set_point(std::complex<T> & point);
  void set_point(std::complex<T> && point);
  void mandelbrot_algorithm();
  bool is_mandelbrot_set();
  unsigned int get_iteration_number();
  //vector<bool> is_mandelbrot_set_optimization(vector<__m256d> & point, vector<int> & nombre_iteration, int max_iteration = MAX_ITERS);
};

#ifndef MANDELBROT_POINT_SOURCE_CPP
#include "Mandelbrot_Point.cpp"
#endif
#endif
