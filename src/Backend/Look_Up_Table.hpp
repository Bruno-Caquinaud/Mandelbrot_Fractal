#ifndef LOOK_UP_TABLE_HEADER_HPP
#define LOOK_UP_TABLE_HEADER_HPP

#include <iostream>
#include <vector>
#include <functional>
#include "opencv2/core.hpp"

#include "constant_backend.hpp"

using namespace std;
using namespace cv;
using namespace Look_Up_Table_Constant;

class Look_Up_Table
{
  vector<Vec3b> table;
  vector<pair<Vec3b, int>> colors;
  function<void(Vec3b &, int, vector<pair<Vec3b, int>> &)> look_up_function;

  public:
  Look_Up_Table(unsigned int size, function<void(Vec3b &, int, vector<pair<Vec3b, int>> &)> function);
  void look_up(unsigned int input, Vec3b & output);
  void init();
  void add_color(pair<Vec3b, int> & color);
  void remove_color();
};

void gradient_color(Vec3b & element, int index, vector<pair<Vec3b, int>>& points);

#endif
