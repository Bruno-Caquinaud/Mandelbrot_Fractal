#ifndef MANDELBROT_VIDEO_HEADER_HPP
#define MANDELBROT_VIDEO_HEADER_HPP

#include <filesystem>
#include "Mandelbrot_Image.hpp"
#include "Video.hpp"
#include "Signals_Slots_Mandelbrot_Video.hpp"

template<typename T>
class Mandelbrot_Video : public Signals_Slots_Mandelbrot_Video
{
  private :

  Mandelbrot_Image<T> image;
  T zoom_max, zoom_step;
  Video video;

  public :

  Mandelbrot_Video();

  void set_zoom(T zoom);
  void set_center_point(std::complex<T> center_point);
  void set_zoom_max(T zoom);
  void set_zoom_step(T zoom);

  Video get_video();
  void create(std::stop_token stop_token);
  void save_video(QString &) override;
};

#ifndef MANDELBROT_VIDEO_SOURCE_CPP
#include "Mandelbrot_Video.cpp"
#endif
#endif
