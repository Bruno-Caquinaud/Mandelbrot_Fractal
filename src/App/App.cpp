#include "App.hpp"

App::App(int argc, char **argv):
context(argc, argv),
view(), image(), video(),
controler(&image, &video, &view)
{
  view.show();
  context.exec();
}
