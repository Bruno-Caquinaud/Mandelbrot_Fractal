#ifndef APP_HEADER_HPP
#define APP_HEADER_HPP

#include <QApplication>
#include "../Controler/Controler.hpp"

class App
{
  private :

  QApplication context;
  MainWindow view;
  Mandelbrot_Image<double> image;
  Mandelbrot_Video<double> video;
  Controler controler;

  public :

  App(int argc, char **argv);
};

#endif
