#ifndef MANDELBROT_POINT_TEST_SOURCE_CPP
#define MANDELBROT_POINT_TEST_SOURCE_CPP
#include "Mandelbrot_Point_Test.hpp"

template<class T>
Mandelbrot_Point_Test<T>::Mandelbrot_Point_Test():Test()
{
  init_jeu_entree();
  init_jeu_sortie();
}

template<class T>
void Mandelbrot_Point_Test<T>::init_jeu_entree_set_point_test()
{
  pair<Mandelbrot_Point<T>, complex<T>> input_1;
  pair<Mandelbrot_Point<T>, complex<T>> input_2;
  pair<Mandelbrot_Point<T>, complex<T>> input_3;
  Mandelbrot_Point<T> mandelbrot_point_1;
  Mandelbrot_Point<T> mandelbrot_point_2;
  Mandelbrot_Point<T> mandelbrot_point_3;

  complex<T> point_1(1, 0);
  complex<T> point_2(0, 1);
  complex<T> point_3(5, 3);

  input_1.first = mandelbrot_point_1;
  input_1.second = point_1;
  input_2.first = mandelbrot_point_2;
  input_2.second = point_2;
  input_3.first = mandelbrot_point_3;
  input_3.second = point_3;

  jeu_entree_set_point_test.push_back(input_1);
  jeu_entree_set_point_test.push_back(input_2);
  jeu_entree_set_point_test.push_back(input_3);
}

template<class T>
void Mandelbrot_Point_Test<T>::init_jeu_entree_mandelbrot_algorithm_test()
{
  tuple<Mandelbrot_Point<T>, complex<T>, complex<T>> input_1;
  tuple<Mandelbrot_Point<T>, complex<T>, complex<T>> input_2;
  tuple<Mandelbrot_Point<T>, complex<T>, complex<T>> input_3;
  tuple<Mandelbrot_Point<T>, complex<T>, complex<T>> input_4;
  Mandelbrot_Point<T> mandelbrot_point_1;
  Mandelbrot_Point<T> mandelbrot_point_2;
  Mandelbrot_Point<T> mandelbrot_point_3;
  Mandelbrot_Point<T> mandelbrot_point_4;
  complex<T> point_1(0 , 0);
  complex<T> point_2(1 , 1);
  complex<T> point_3(0 , 0);
  complex<T> point_4(1 , 1);
  complex<T> suit_value_1(0, 0);
  complex<T> suit_value_2(0, 0);
  complex<T> suit_value_3(1, 1);
  complex<T> suit_value_4(1, 1);

  get<0>(input_1) = mandelbrot_point_1;
  get<1>(input_1) = point_1;
  get<2>(input_1) = suit_value_1;

  get<0>(input_2) = mandelbrot_point_2;
  get<1>(input_2) = point_2;
  get<2>(input_2) = suit_value_2;

  get<0>(input_3) = mandelbrot_point_3;
  get<1>(input_3) = point_3;
  get<2>(input_3) = suit_value_3;

  get<0>(input_4) = mandelbrot_point_4;
  get<1>(input_4) = point_4;
  get<2>(input_4) = suit_value_4;

  jeu_entree_mandelbrot_algorithm_test.push_back(input_1);
  jeu_entree_mandelbrot_algorithm_test.push_back(input_2);
  jeu_entree_mandelbrot_algorithm_test.push_back(input_3);
  jeu_entree_mandelbrot_algorithm_test.push_back(input_4);
}

template<class T>
void Mandelbrot_Point_Test<T>::init_jeu_entree_is_mandelbrot_set_test()
{
  pair<Mandelbrot_Point<T>, complex<T>> input_1;
  pair<Mandelbrot_Point<T>, complex<T>> input_2;
  pair<Mandelbrot_Point<T>, complex<T>> input_3;
  Mandelbrot_Point<T> mandelbrot_point_1;
  Mandelbrot_Point<T> mandelbrot_point_2;
  Mandelbrot_Point<T> mandelbrot_point_3;
  complex<T> point_1(0 , 0);
  complex<T> point_2(1 , 0);
  complex<T> point_3(3 , 4);

  input_1.first = mandelbrot_point_1;
  input_1.second = point_1;

  input_2.first = mandelbrot_point_2;
  input_2.second = point_2;

  input_3.first = mandelbrot_point_3;
  input_3.second = point_3;

  jeu_entree_is_mandelbrot_set_test.push_back(input_1);
  jeu_entree_is_mandelbrot_set_test.push_back(input_2);
  jeu_entree_is_mandelbrot_set_test.push_back(input_3);
}

template<class T>
void Mandelbrot_Point_Test<T>::init_jeu_sortie_set_point_test()
{
  complex<T> output_1(1, 0);
  complex<T> output_2(0, 1);
  complex<T> output_3(5, 3);

  jeu_sortie_set_point_test.push_back(output_1);
  jeu_sortie_set_point_test.push_back(output_2);
  jeu_sortie_set_point_test.push_back(output_3);
}

template<class T>
void Mandelbrot_Point_Test<T>::init_jeu_sortie_mandelbrot_algorithm_test()
{
  complex<T> output_1(0, 0);
  complex<T> output_2(1, 1);
  complex<T> output_3(0, 2);
  complex<T> output_4(1, 3);

  jeu_sortie_mandelbrot_algorithm_test.push_back(output_1);
  jeu_sortie_mandelbrot_algorithm_test.push_back(output_2);
  jeu_sortie_mandelbrot_algorithm_test.push_back(output_3);
  jeu_sortie_mandelbrot_algorithm_test.push_back(output_4);
}

template<class T>
void Mandelbrot_Point_Test<T>::init_jeu_sortie_is_mandelbrot_set_test()
{
  bool input_1 = true;
  bool input_2 = true;
  bool input_3 = false;

  jeu_sortie_is_mandelbrot_set_test.push_back(input_1);
  jeu_sortie_is_mandelbrot_set_test.push_back(input_2);
  jeu_sortie_is_mandelbrot_set_test.push_back(input_3);
}

template<class T>
void Mandelbrot_Point_Test<T>::init_jeu_entree()
{
  init_jeu_sortie_set_point_test();
  init_jeu_entree_mandelbrot_algorithm_test();
  init_jeu_entree_is_mandelbrot_set_test();
}

template<class T>
void Mandelbrot_Point_Test<T>::init_jeu_sortie()
{
  init_jeu_sortie_set_point_test();
  init_jeu_sortie_mandelbrot_algorithm_test();
  init_jeu_sortie_is_mandelbrot_set_test();
}

template<class T>
void Mandelbrot_Point_Test<T>::test()
{
  set_point_test();
  mandelbrot_algorithm_test();
  is_mandelbrot_set_test();
}

template<class T>
void Mandelbrot_Point_Test<T>::set_point_test()
{
  if(jeu_entree_set_point_test.size() != jeu_sortie_set_point_test.size())
  {
    typename vector<pair<Mandelbrot_Point<T>, complex<T>>>::iterator input = jeu_entree_set_point_test.begin();
    typename vector<complex<T>>::iterator output =  jeu_sortie_set_point_test.begin();

    for(;input != jeu_entree_set_point_test.end()
        && output != jeu_sortie_set_point_test.end();
        input++, output++)
    {
      Mandelbrot_Point<T> & tested_point = input->first;
      bool success = false;

      tested_point.set_point(input->second);

      if(tested_point.point == *output)
      {
        success = true;
      }

      set_point_test_success.push_back(success);
    }
  }
}

template<class T>
void Mandelbrot_Point_Test<T>::mandelbrot_algorithm_test()
{
  if(jeu_entree_mandelbrot_algorithm_test.size() == jeu_sortie_mandelbrot_algorithm_test.size())
  {
    typename vector<tuple<Mandelbrot_Point<T>, complex<T>, complex<T>>>::iterator input = jeu_entree_mandelbrot_algorithm_test.begin();
    typename vector<complex<T>>::iterator output =  jeu_sortie_mandelbrot_algorithm_test.begin();

    for(;input != jeu_entree_mandelbrot_algorithm_test.end()
        && output != jeu_sortie_mandelbrot_algorithm_test.end();
        input++, output++)
    {
      Mandelbrot_Point<T> & tested_point = get<0>(*input);
      bool success = false;

      tested_point.point = get<1>(*input);
      tested_point.suit_value = get<2>(*input);

      tested_point.mandelbrot_algorithm();

      if(tested_point.suit_value == *output)
      {
        success = true;
      }

      mandelbrot_algorithm_test_success.push_back(success);
    }
  }
}

template<class T>
void Mandelbrot_Point_Test<T>::is_mandelbrot_set_test()
{
  if(jeu_entree_is_mandelbrot_set_test.size() == jeu_sortie_is_mandelbrot_set_test.size())
  {
    typename vector<pair<Mandelbrot_Point<T>, complex<T>>>::iterator input = jeu_entree_is_mandelbrot_set_test.begin();
    vector<bool>::iterator output =  jeu_sortie_is_mandelbrot_set_test.begin();

    for(;input != jeu_entree_is_mandelbrot_set_test.end()
        && output != jeu_sortie_is_mandelbrot_set_test.end();
        input++, output++)
    {
      Mandelbrot_Point<T> & tested_point = input->first;
      bool success = false;

      tested_point.point = input->second;

      if(tested_point.is_mandelbrot_set() == *output)
      {
        success = true;
      }

      is_mandelbrot_set_success.push_back(success);
    }
  }
}

#endif
