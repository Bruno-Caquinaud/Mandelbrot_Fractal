#ifndef TEST_HEADER_HPP
#define TEST_HEADER_HPP

#include <vector>
#include <any>

using namespace std;

class Test
{
  protected :

  bool success;

  virtual void init_jeu_entree() = 0;
  virtual void init_jeu_sortie() = 0;

  public :

  Test():success(false){};
  virtual ~Test() = default;
  virtual void test() = 0;
  bool get_success(){return success;}
};

#endif
