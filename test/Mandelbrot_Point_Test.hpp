#ifndef MANDELBROT_POINT_TEST_HEADER_HPP
#define MANDELBROT_POINT_TEST_HEADER_HPP

#include <vector>
#include <complex>
#include <utility>
#include <tuple>
#include "Test.hpp"
#include "../src/Backend/Mandelbrot_Point.hpp"

using namespace std;

template<class T>
class Mandelbrot_Point_Test : public Test
{
  private:

  vector<bool> set_point_test_success;
  vector<bool> mandelbrot_algorithm_test_success;
  vector<bool> is_mandelbrot_set_success;

  vector<pair<Mandelbrot_Point<T>, complex<T>>> jeu_entree_set_point_test;
  vector<complex<T>> jeu_sortie_set_point_test;
  vector<tuple<Mandelbrot_Point<T>, complex<T>, complex<T>>> jeu_entree_mandelbrot_algorithm_test;
  vector<complex<T>> jeu_sortie_mandelbrot_algorithm_test;
  vector<pair<Mandelbrot_Point<T>, complex<T>>> jeu_entree_is_mandelbrot_set_test;
  vector<bool> jeu_sortie_is_mandelbrot_set_test;

  void init_jeu_entree_set_point_test();
  void init_jeu_entree_mandelbrot_algorithm_test();
  void init_jeu_entree_is_mandelbrot_set_test();
  void init_jeu_sortie_set_point_test();
  void init_jeu_sortie_mandelbrot_algorithm_test();
  void init_jeu_sortie_is_mandelbrot_set_test();

  protected :

  void init_jeu_entree() override;
  void init_jeu_sortie() override;

  public:

  Mandelbrot_Point_Test();

  void test() override;
  void set_point_test();
  void mandelbrot_algorithm_test();
  void is_mandelbrot_set_test();
};

#ifndef MANDELBROT_POINT_TEST_SOURCE_CPP
#include "Mandelbrot_Point_Test.cpp"
#endif
#endif
