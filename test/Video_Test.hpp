#ifndef VIDEO_TEST_HEADER_HPP
#define VIDEO_TEST_HEADER_HPP

#include <vector>
#include <list>
#include <utility>
#include <opencv2/core.hpp>

#include "Test.hpp"
#include "../src/Backend/Video.hpp"

using namespace std;
using namespace cv;

class Video_Test : public Test
{
  private :

  vector<bool> get_images_list_test_success;

  vector<Video> jeu_entree_get_images_list_test;
  vector<list<Mat>> jeu_sortie_get_images_list_test;

  void init_jeu_entree_get_images_list_test();
  void init_jeu_sortie_get_images_list_test();

  protected :

  void init_jeu_entree() override;
  void init_jeu_sortie() override;

  public :

  Video_Test();
  void test() override;
  void get_images_list_test();
};

#endif
