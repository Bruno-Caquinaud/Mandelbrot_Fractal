#ifndef MANDELBROT_APP_TEST_HEADER_HPP
#define MANDELBROT_APP_TEST_HEADER_HPP

#include <memory>
#include "Mandelbrot_Point_Test.hpp"
#include "Video_Test.hpp"

template<class T>
class Mandelbrot_App_Test
{
  private :

  vector<unique_ptr<Test>> test_list;
  void add_test();

  public :

  Mandelbrot_App_Test();
  void run();
};

#ifndef MANDELBROT_APP_TEST_SOURCE_CPP
#include "Mandelbrot_App_Test.cpp"
#endif
#endif
