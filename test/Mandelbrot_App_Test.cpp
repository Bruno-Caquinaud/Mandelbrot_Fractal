#ifndef MANDELBROT_APP_TEST_SOURCE_CPP
#define MANDELBROT_APP_TEST_SOURCE_CPP
#include "Mandelbrot_App_Test.hpp"

template<class T>
Mandelbrot_App_Test<T>::Mandelbrot_App_Test()
{
  add_test();
}

template<class T>
void Mandelbrot_App_Test<T>::add_test()
{
  test_list.push_back(make_unique<Mandelbrot_Point_Test<T>>());
  test_list.push_back(make_unique<Video_Test>());
}

template<class T>
void Mandelbrot_App_Test<T>::run()
{
  for(unique_ptr<Test> & test : test_list)
  {
    test->test();
  }
}

#endif
