#include "Video_Test.hpp"

Video_Test::Video_Test():Test()
{
  init_jeu_entree();
  init_jeu_sortie();
}

void Video_Test::init_jeu_entree_get_images_list_test()
{
  Video input_1;
  Video input_2;
  Mat image_1 = Mat::zeros(3, 3, CV_8U);

  input_2.images.push_back(image_1);
  jeu_entree_get_images_list_test.push_back(input_1);
  jeu_entree_get_images_list_test.push_back(input_2);
}

void Video_Test::init_jeu_sortie_get_images_list_test()
{
  list<Mat> input_1;
  list<Mat> input_2;
  Mat image_1 = Mat::zeros(3, 3, CV_8U);

  input_2.push_back(image_1);
  jeu_sortie_get_images_list_test.push_back(input_1);
  jeu_sortie_get_images_list_test.push_back(input_2);
}

void Video_Test::init_jeu_entree()
{
  init_jeu_entree_get_images_list_test();
}

void Video_Test::init_jeu_sortie()
{
  init_jeu_sortie_get_images_list_test();
}

void Video_Test::get_images_list_test()
{
  if(jeu_entree_get_images_list_test.size() == jeu_sortie_get_images_list_test.size())
  {
    vector<Video>::iterator inputs = jeu_entree_get_images_list_test.begin();
    vector<list<Mat>>::iterator outputs = jeu_sortie_get_images_list_test.begin();

    for(; inputs != jeu_entree_get_images_list_test.end()
        && outputs != jeu_sortie_get_images_list_test.end();
      inputs++, outputs++)
    {
      bool success = false;
      if(inputs->get_images_list().size() == outputs->size())
      {
        list<Mat>::iterator input_list_image = inputs->get_images_list().begin();
        list<Mat>::iterator output_list_image = outputs->begin();

        for(; input_list_image != inputs->get_images_list().end()
          && output_list_image != outputs->end();
          input_list_image++, output_list_image++)
        {
          success = equal(input_list_image->begin<uchar>(), input_list_image->end<uchar>(), output_list_image->begin<uchar>());
        }
      }

      get_images_list_test_success.push_back(success);
    }
  }
}

void Video_Test::test()
{
  get_images_list_test();
}
