#include "main.hpp"

int main()
{
  Mandelbrot_App_Test<float> mandelbrot_test;

  mandelbrot_test.run();
  return 0;
}
