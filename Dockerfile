FROM ubuntu

ENV TZ="Europe/London"
ENV DEBIAN_FRONTEND="noninteractive"

RUN apt-get update -y
RUN apt-get install gcc-10 -y
RUN apt-get install g++-10 -y
RUN apt-get install cmake -y
RUN apt-get install git -y
RUN apt-get install -y libtbb2
RUN apt-get install -y libtbb-dev

WORKDIR /bin

RUN rm gcc gcc-ar gcc-nm gcc-ranlib
RUN ln -s g++-10 g++
RUN ln -s gcc-10 gcc
RUN ln -s gcc-ar-10 gcc-ar
RUN ln -s gcc-nm-10 gcc-nm
RUN ln -s gcc-ranlib-10 gcc-ranlib

RUN mkdir -p /home/bruno/lib
WORKDIR /home/bruno/lib
RUN git clone https://github.com/opencv/opencv.git
RUN mkdir -p opencv/build
WORKDIR opencv/build
RUN cmake -D CMAKE_BUILD_TYPE=RELEASE -D CMAKE_INSTALL_PREFIX=/usr/local -D WITH_TBB=ON -D WITH_V4L=ON -D OPENCV_PYTHON3_INSTALL_PATH=$cwd/OpenCV-$cvVersion-py3/lib/python3.5/site-packages -D WITH_FFMPEG=ON -D WITH_QT=ON -D WITH_OPENGL=ON -DBUILD_LIST=core,highgui,improc,dnn,features2d,imgcodecs,videoio ..
RUN make -j4
RUN make install
WORKDIR /home/bruno
RUN rm -rf *

RUN apt-get install -y qt5-default
RUN apt-get install -y libqt5multimediawidgets5
RUN apt-get install -y libqt5multimedia5-plugins
RUN apt-get install -y qttools5-dev
RUN apt-get install -y qtmultimedia5-dev
RUN apt-get install -y valgrind
RUN ldconfig
